<?php

$conexion = new mysqli('localhost', 'root', 'x', 'articulos');
if ( $conexion->connect_error){ die('Error de conexión'); }
$conexion->set_charset('utf8');
header('Access-Control-Allow-Origin: *');
header('Content-type: application/json; charset=utf-8'); 


  function listar(){  
     global $conexion;
    $data = $conexion->query('select * from autores order by nombre');
    $resp = [];
    while ( $doc = $data->fetch_assoc() ){ array_push($resp, $doc); }
    echo json_encode($resp);
   } 
    
   function insertar(){    
    global $conexion;
    $datos = json_decode($_POST['datos']);
    $sql = $conexion->prepare('insert into autores (id_autor, apellidos, nombre, email ) values (0, ?, ?, ?) ');
    $sql->bind_param('sss',  $datos->apellidos, $datos->nombre,  $datos->email);
    echo $sql->execute();
}

function actualizar(){    
    global $conexion;
    $datos = json_decode($_POST['datos']);
    $sql = $conexion->prepare('update autores set apellidos = ?, nombre = ?,  email = ? where id_autor = ?');
    $sql->bind_param('sssi',  $datos->apellidos, $datos->nombre,  $datos->email, $datos->id_autor);
    echo $sql->execute();
}

function borrar(){    
  global $conexion;
  $datos = json_decode($_POST['datos']);
  $sql = $conexion->prepare('delete from autores where id_autor = ?');
  $sql->bind_param('i', $datos->id);
  echo $sql->execute();
}

   switch ($_POST['accion']) {

    case 'listar':
      listar();
      break;
   
    case 'insertar':   
      insertar();
      break;   
   
    case 'actualizar':   
      actualizar();
      break;      
   
   case 'borrar':   
      borrar();
      break;      
   
   }
?>



 